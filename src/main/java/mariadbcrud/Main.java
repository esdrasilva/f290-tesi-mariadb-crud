package mariadbcrud;

import mariadbcrud.dao.ConnectionFactory;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {

        //TODO: Remover trecho apos validar conexao com o banco de dados.
        String query = "SELECT VERSION();";
        try (Statement st = ConnectionFactory.getConexao().createStatement();
             ResultSet rs = st.executeQuery(query)) {
            if (rs.next()) {
                System.out.println(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    //TODO: Realizar testes de inclusão, leitura, remoção e atualização de dados na entidade [ user ].
}
