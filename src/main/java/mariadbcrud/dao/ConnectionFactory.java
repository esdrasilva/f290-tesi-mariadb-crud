package mariadbcrud.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {
    public static Connection getConexao() {
        String database = "fatec";
        String url = "jdbc:mysql://localhost:3306/"+database+"?useSSL=false";
        //TODO: Adicionar as credenciais de seu banco de dados
        String user = "aluno";
        String password = "fatec";

        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            Logger lgr = Logger.getLogger(ConnectionFactory.class.getName());
            lgr.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    //TODO: Realizar sobrescrita de método  [ getConexao() ] para receber um aparametro [ ConnectionInfo ] e subtibituir os dados da url de conexão pelos dados do parametro recebido.
}
